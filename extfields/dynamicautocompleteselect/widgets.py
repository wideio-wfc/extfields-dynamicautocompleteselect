# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import uuid
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe

import  settings
import sys
import json


def getuuid():
    return str(uuid.uuid1()).replace('-','')


class DynamicAutocompleteSelectWidget(Widget):
    allow_multiple_selected = True

    def __init__(
            self,
            attrs={},
            multiple=True,
            model=None,
            humanid=None,
            imageid=None,
            choices=(),
            *args,
            **kwargs):
        self.attrs = attrs
        self.model = model
        self.humanid = (
            humanid if humanid is not None else (
                model.WIDEIO_Meta.search_enabled if hasattr(
                    model,
                    "WIDEIO_Meta") and hasattr(
                    model.WIDEIO_Meta,
                    "search_enabled") else "name"))
        self.imageid = imageid
        self.multiple = multiple
        self.choices = list(choices)

    def render_option(self, label, id):
        return json.dumps([label, id])

    def value_from_datadict(self, data, files, name):
        if name not in data:
            #sys.stderr.write("%r not in data : %r %r\n"%(name,data,files))
            return []
        return (data.getlist(name) if hasattr(data,"getlist") else [data[name]])

    def render(self, name, value, attrs=None, choices=()):
        # from wioframework.uuid import getuuid
        widgettemplate = loader.get_template(
            'extfields/dynamic_autocomplete_select.html')
        object_class = self.model
        print "DynamicAutocompleteSelectWidget render :: ", name, value, self.choices
        if value is None:
            value = ""
        # elif isinstance(value, tuple):
        #     q = {value[0].model.__name__.lower()+"_id":value[1]}
        #     value = ",".join([getattr(x,value[0].rel.to.__name__.lower) for x in value[0].rel.through.objects.filter(**q)])
        elif isinstance(value, list):
            selected = []
            for id, label in self.choices:
                if id in value:
                    selected.append([label, id])
            value = json.dumps(selected)
            # value = "[" + ",".join([(x.id if hasattr(x,"id") else x)+","+unicode(x) for x in value]) + "]"

        html_content = widgettemplate.render(Context({
            'name': name,
            'value': value,
            'widgetuuid': getuuid(),
            'object_class': object_class,
            'objectlist': self.choices,
            'multiple': self.multiple,
            'humanid': self.humanid,
            'imageid': self.imageid,
            'MEDIA_URL': settings.MEDIA_URL
        }))
        return mark_safe(html_content)
